import React, { Component } from 'react'

function Welcome(props) {
    return <h1>Hello, {props.name}</h1>;
  }
  
  function formatDate(date) {
    return date.toLocaleDateString();
  }
  
  function Comment(props) {
    return (
      <div className="Comment">
        <div className="UserInfo">
          <img
            className="Avatar"
            src={props.author.avatarUrl}
            alt={props.author.name}
          />
          <div className="UserInfo-name">
            {props.author.name}
          </div>
        </div>
        <div className="Comment-text">{props.text}</div>
        <div className="Comment-date">
          {formatDate(props.date)}
        </div>
      </div>
    );
  }
  
  const comment = {
    date: new Date(),
    text: 'I hope you enjoy learning React!',
    author: {
      name: 'Hello Kitty',
      avatarUrl: 'https://placekitten.com/g/64/64',
    },
  };
  
  class Clock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {date: new Date()};
    }
  
    componentDidMount() {
      this.timerID = setInterval(
        () => this.tick(),
        1000
      );
    }
  
    componentWillUnmount() {        
      clearInterval(this.timerID);    
    }
  
    tick() {
      this.setState({
        date: new Date()
      });
    }
  
    render() {
      return (
        <div>
          <h1>Hello, world!</h1>
          <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
        </div>
      );
    }
  }
  
  function ActionLink() {
    function handleClick(e) {
      e.preventDefault();
      console.log('The link was clicked.');
    }
  
    return (
      <a href="#" onClick={handleClick}>
        Click me
      </a>
    );
  }
  
  class Toggle extends React.Component {
    constructor(props) {
      super(props);
      this.state = {isToggleOn: true};
  
      // This binding is necessary to make `this` work in the callback
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick() {
      this.setState(state => ({
        isToggleOn: !state.isToggleOn
      }));
    }
  
    render() {
      return (
        <button onClick={this.handleClick}>
          {this.state.isToggleOn ? 'ON' : 'OFF'}
        </button>
      );
    }
  }
  
  class LoginButton extends React.Component {
    handleClick() {    
      console.log('this is:', this);
    }
    deleteRow() {
      console.log(this)
    }
  
    render() {
      const id = "01";
      // This syntax ensures `this` is bound within handleClick
      return (
        <div>
          <button onClick={(e) => this.handleClick(e)}>
            Click me
          </button>
          <button onClick={(e) => this.deleteRow(id, e)}>Delete Row</button>
          <button onClick={this.deleteRow.bind(this, id)}>Delete Row</button>
        </div>
      );
    }
  }
  
  function UserGetting(props) {
    return <h1>Welcome back.!</h1>
  }
  
  function GuestGetting(props) {
    return <h1>Please sign up</h1>
  }
  
  function Getting(props) {
    const isLoggedIn = props.isLoggedIn;
    if(isLoggedIn){
      return <UserGetting />
    }
    return <GuestGetting />
  }
  
  function Loginbutton(props) {
    return (
      <button onClick={props.onClick}>
        Login
      </button>
    );
  }
  
  function Logoutbutton(props) {
    return (
      <button onClick={props.onClick}>
        Logout
      </button>
    );
  }
  
  class LoginController extends React.Component {
    constructor(props){
      super(props);
      this.handleLoginClick = this.handleLoginClick.bind(this);
      this.handleLogoutClick = this.handleLogoutClick.bind(this);
      this.state = {isLoggedIn: false}
    }
  
    handleLoginClick() {
      this.setState({isLoggedIn: true})
    }
    
    handleLogoutClick() {
      this.setState({isLoggedIn: false})
    }
  
    render(){
      const isLoggedIn = this.state.isLoggedIn;
      let button;
  
      if(isLoggedIn) {
        button = <Logoutbutton onClick={this.handleLogoutClick} />
      } else {
        button = <Loginbutton onClick={this.handleLoginClick} />
      }
  
      return <div>
          <Getting isLoggedIn={isLoggedIn} />
          {button}
        </div>
    }
  }
  
  function MailBox(props) {
    const msgUnread = props.msgUnread;
  
    return <div>
      <h1>Hello</h1>
      {msgUnread.length > 0 && 
      <h3> You have {msgUnread.length} messages unread</h3>
      }
    </div>
  }
  
  const messages = ['React', 'Re: React', 'Re:Re: React'];
  
  const numbers = [1,2,3,4,5];
  
  function ListNumber(props){
    const number = props.numbers.map((number, index) => <li key={index}>{number}</li>)
    return <ul>
      {number}
    </ul>
  }
  
  function BoilingVerdict(props) {
    if (props.celsius >= 100) {
      return <p>The water would boil.</p>;
    }
    return <p>The water would not boil.</p>;
  }
  
  class Calculator extends React.Component {
    constructor(props){
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.state ={temperature:''};
    }
  
    handleChange(e){
      this.setState({temperature: e.target.value})
    }
  
    render(){
      const temperature = this.state.temperature
      return (
        <form>
          <fieldset>
            <legend>Enter temperature in celsius</legend>
            <BoilingVerdict celsius={parseFloat(temperature)} />
            <input value={temperature} onChange={this.handleChange}></input>
          </fieldset>
        </form>
      )
    }
  }
  
  function FancyBorder(props) {
    return (
      <div className={'FancyBorder FancyBorder-' + props.color}>
        {props.children}
      </div>
    );
  }
  
  function WelcomeDialog() {
    return (
      <FancyBorder color="blue">
        <h1 className="Dialog-title">
          Welcome
        </h1>
        <p className="Dialog-message">
          Thank you for visiting our spacecraft!
        </p>
      </FancyBorder>
    );
  }

export default class TextConcept extends Component {
  render() {
    return (
        <div>
            <WelcomeDialog />
            <Calculator />                  
            <ListNumber numbers={numbers}/>      
            <MailBox msgUnread={messages}/>
            <LoginController />
            <Getting isLoggedIn={false}/>
            <LoginButton />
            <Toggle />
            <ActionLink />
            <Welcome name="Sara" />
            <Welcome name="Cahal" />
            <Welcome name="Edite" />            
            <Comment 
                date={comment.date}
                text={comment.text}
                author={comment.author}
            />
            <Clock />
        </div>
    )
  }
}
