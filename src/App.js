import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Container, Row, Col, ListGroup, Modal } from 'react-bootstrap';

const taskData = [
  {
      "title": "working 1",
      "description": "bla bla bla.....",
      "avatar": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJvdLsIJS8Ez8MC3w4Hbc-Jsbe8delPXSSFlSEfti_R36MH0W6",
      "status": "TO DO"
  },
  {
      "title": "working 1",
      "description": "bla bla bla.....",
      "avatar": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSrQPi7BPJAXGM4PQtSxMw9zqat6E2m7H5DheH8XqtquyeYzsSSSg",
      "status": "IN PROGRESS"
  },
  {
      "title": "working 1",
      "description": "bla bla bla.....",
      "avatar": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSE_mlLRp0i2CeaKD_4ygEELbcfLPqU14sfpmxSbl6revUcoYT4",
      "status": "IN REVIEW"
  }
]


function ContainerCol(){
  return(
    <Col xs>
      <h4>TO DO 4 of 47</h4>
      
    </Col>
  )
}

function App() {
  return (
    <Container>
      <Row>
        <ContainerCol/>        
        <Col xs={{ order: 12 }}>IN PROGRESS 2 of 11</Col>
        <Col xs={{ order: 1 }}>IN REVIEW 2 of 31</Col>
      </Row>
    </Container>
    
  );
}

export default App;
